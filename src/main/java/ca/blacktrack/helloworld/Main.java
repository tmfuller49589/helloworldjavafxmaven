package ca.blacktrack.helloworld;

import java.lang.Runtime.Version;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
	private Stage primaryStage;

	public Main() {

	}

	@Override
	public void start(Stage primaryStage) {
		Button btn = new Button();
		btn.setText("Say 'Hello World'");
		btn.setOnAction(event -> System.out.println("Hello World!"));

		StackPane root = new StackPane();
		root.getChildren().add(btn);

		Scene scene = new Scene(root, 300, 250);

		primaryStage.setTitle("Hello World!");
		primaryStage.setScene(scene);
		primaryStage.show();
		Version version = java.lang.Runtime.version();
		System.out.println("Java Version = " + version);
	}

	public static void main(String[] args) {
		launch(args);
	}

}
